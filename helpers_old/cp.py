from thrd import ThreadUp
from shutil import copytree


class FastCopy(object):
    '''
    This class is made to copy files faster using threads
    '''
    @staticmethod
    @ThreadUp
    def mcopy(folderO, folderD):
        '''
        Take as arguments the origin folder, destiny folder and the number
        of files that is going to copy
        '''
        try:
            copytree(folderO, folderD)
        except Exception:
            pass


if __name__ == '__main__':
    c = FastCopy()
    c.mcopy(str(input("Introduce la carpeta de origen: ")),
            str(input("Carpeta destino: ")))
