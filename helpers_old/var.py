from jreader import reader
from os.path import exists


class Vars(object):
    def __init__(self):
        self.usb = ".db/.usb.json"
        self.pointer = ".db/.pointer.json"
        self.v = dict(
            windows=['E', 'F', 'G', 'H', 'I', 'J'],
            unix=dict(
                Darwin="/Volumes/",
                Linux="/media/",
                usb_labels=[]),
            pointer={}
        )

    @property
    def update(self):
        if exists(self.usb) and exists(self.pointer):
            self.v["unix"]["usb_labels"] = reader("usb")["name"]
            self.v["pointer"] = reader("pointer")["usb"][0]

        elif exists(self.usb):
            self.v["unix"]["usb_labels"] = reader("usb")["name"]

        elif exists(self.pointer):
            self.v["pointer"] = reader("pointer")["usb"][0]

        return self.v