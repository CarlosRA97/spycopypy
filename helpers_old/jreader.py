import json
import os

folder = ".db/"
filetype = ".json"
h = "."

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath).split("/")
index = dname.index("helpers")
dname[index] = folder
folder = "/".join(dname)

h = folder + h + "{0}" + filetype


def create_if_not():
    if not os.path.exists(folder):
        os.mkdir(folder)


def reader(file):
    """
    reader( filename of the json file )
    """
    # createIfNot()
    try:
        with open(h.format(file), 'r+') as data_file:
            data = json.load(data_file)

        return data
    except Exception:
        pass


def writer(file, field, i=None):
    """
    writer(filename of the json file , field in json file, data to write)
    """
    create_if_not()
    try:
        with open(h.format(file), 'r+') as f:
            data = json.load(f)
            if (type(data[field]) is list) and (i not in data[field]):
                data[field].append(i)
            else:
                # print "%s already on file" % i
                return True

            f.seek(0)  # <--- should reset file position to the beginning.
            json.dump(data, f, indent=4, skipkeys=True)

    except IOError:
        with open(h.format(file), 'w+') as newFile:
            data = {field: [i]}
            json.dump(data, newFile, indent=4, skipkeys=True)
