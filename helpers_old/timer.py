class T(object):

    def __init__(self):
        from time import sleep
        self.sleep = sleep
        self.Sec = 0
        self.Min = 0
        self.Hour = 0

    def timer(self):
        while True:
            self.Sec += 1
            self.sleep(1)

            if self.Sec == 60:
                self.Sec = 0
                self.Min += 1

            elif self.Min == 60:
                self.Min = 0
                self.Hour += 1

            yield (self.Hour, self.Min, self.Sec)
