import os

from cp import FastCopy
from jreader import reader

class Copyprocess():
    def __init__(self):
        self.copy()

    @staticmethod
    def f(folder):
        abspath = os.path.abspath(__file__)
        dname = os.path.dirname(abspath).split("/")
        index = dname.index("helpers")
        dname[index] = folder
        folder = "/".join(dname)
        return folder

    def copy(self):
        begin = FastCopy()
        self.create_if_not(self.f(".out/"))

        if os.path.exists(self.f(".db/.tcp.json")):
            folder_list = reader("tcp")["folders"]

            print(folder_list)
            for dire in folder_list:
                begin.mcopy(dire, self.f(".out/") + dire)
        else:
            return False

    @staticmethod
    def create_if_not(f):
        if not os.path.exists(f):
            os.mkdir(f)


if __name__ == '__main__':
    copyprocess()
