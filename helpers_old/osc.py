from os import walk, remove, path
from platform import system

from jreader import writer
from var import Vars


class MainWorker(Vars):
	def __init__(self):
		super(MainWorker, self).__init__()
		self.sys = system()
		self.windows = self.v["windows"]
		self.unix = self.v["unix"]
		self.usb_labels = self.unix["usb_labels"]
		self.unix_root = self.unix[self.sys]
		self.dict_worker = dict(
			Linux=dict(root=self.unix_root, usbs=list(), id=3),
			Darwin=dict(root=self.unix_root, usbs=list(), id=2),
			Windows=dict(usbs=self.windows, id=1)
		)
		self.system = self.dict_worker[self.sys]

	def main_worker(self):
		if self.sys == "Windows":
			self.universal_worker(usbs=self.system["usbs"], id=self.system["id"])
		else:
			self.system["usbs"] = self.update["unix"]["usb_labels"]
			self.universal_worker(self.system["root"], self.system["usbs"], self.system["id"])

	def universal_worker(self, root=None, usbs=None, id=None):
		root_dir = ""
		for usb in usbs:
			if id == 1:
				root_dir = '{0}:/'.format(usb)
			elif id == 2 or 3:
				root_dir = root + '{0}'.format(usb)
			self.searchtree(root_dir, usb)

	def searchtree(self, root_dir, usbi):
		''' Look up recursively down into a folder, it has a copy function '''
		count = 0
		dict_pointer = self.update["pointer"]
		try:
			usb_pointer = dict_pointer[usbi]
		except KeyError:
			usb_pointer = []

		docs = list()
		for dirName, subdirList, fileList in walk(root_dir):

			# This two lines down here, clean the arrays of hidden files
			fileList = [f for f in fileList if not f[0] == '.']
			subdirList[:] = [d for d in subdirList if not d[0] == '.']
			if usb_pointer and count in usb_pointer:
				writer("tcp", "folders", dirName)

			container = "{0}#({1})".format(dirName, count)

			count += 1

			f_contained = []
			for fname in fileList:
				f_contained.append("{0}".format(fname))

			doc = {container: f_contained}
			docs.append(doc)

		fdoc = {usbi: docs}
		writer("folder", "usb", fdoc)

		try:
			if path.exists(".db/.tcp.json"):
				remove(".db/.pointer.json")
		except Exception:
			pass
