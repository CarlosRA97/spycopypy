from thrd import ThreadUp


@ThreadUp
def update_usb():
    from os import path, listdir
    from platform import system
    from helpers.jreader import writer

    dict_unix = dict(Darwin="/Volumes/",
                     Linux="/media/")
    root_dir = None

    if system() != "Windows":
        root_dir = dict_unix[system()]

    drives = listdir(root_dir)
    drives = [f for f in drives if not f[0] == '.']
    for usb in drives:
        if path.ismount(root_dir + usb):
            if writer("usb", "name", usb):
                pass
