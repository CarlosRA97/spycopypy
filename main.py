#!/usr/bin/env python

from subprocess import call

from helpers.helpers import *


class Main(MainWorker, T, Copyprocess, JsonIO, CommonWealth):
	def __init__(self):
		super(Main, self).__init__()
		self.scanner()

	@property
	def iswindows(self):
		if self.sys == "Windows":
			return True
		else:
			return False

	def cexit(self, m):
		if m == self.exittime:
			if not self.iswindows:
				call(self.delhis)
			quit()

		elif m == self.copytime:
			self.copy()

	@ThreadUp
	def child_worker(self, m, s):
		if m in self.exct and s == 1:
			self.main_worker()

	def scanner(self):
		for h, m, s in self.timer:
			print("{h}h {m}m {s}s".format(h=h, m=m, s=s))
			if not self.iswindows:
				self.update_usb(self)
				self.child_worker(self, m, s)
				self.cexit(m)
			else:
				self.child_worker(self, m, s)
				self.cexit(m)


if __name__ == '__main__':
	Main()
