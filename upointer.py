from helpers.jreader import writer
from helpers.var import Vars
from os import remove


def pg(p):

    """
    typedef: None
    """
    data_given = p.split(":")

    name = data_given[0]
    str_pointers = list(set(data_given[1].split(",")))
    pointers = list()
    for i in str_pointers:
        pointers.append(int(i))

    try:
        new = Vars().v["pointer"][0][name] + pointers
    except KeyError:
        new = pointers

    print(new)
    try:
        remove(".db/.pointer.json")
    except Exception:
        pass

    writer("pointer", "usb", {name: new})


data = raw_input("Pointers?: ")
pg(data)
